<?php

/*Route::post('cart','');*/
Route::resource('products', "ProductController");
Route::resource('categories', "CategoryController");
Route::get('/', "ProductController@index");
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
