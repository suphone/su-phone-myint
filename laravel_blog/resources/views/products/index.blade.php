@extends('layout')

@section('content')
    <a href="{{ url('products/create') }}" class="btn btn-primary">Create New Product</a>
    
    <hr>
    
    @foreach($products as $product)
        <h3>
            <a href="{{ url('products', $product->id) }}">
                {{ $product->name }}
            </a>
        </h3>

        <p>
            {{ $product->description }}
        </p>
        <p>Price - <span class="badge badge-success">$ {{  $product->price }}</span></p>
        <p>Quantity - <span class="badge badge-danger">{{ $product->quantity }}</span></p>

        <form action="{{ route('products.destroy', $product->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <a href="{{ route('products.edit', $product->id) }}" class="btn btn-info">Edit</a>
            <button class="btn btn-danger">Delete</button>
        </form>
        <hr>
    @endforeach
@endsection