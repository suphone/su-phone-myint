@extends('layout')

@section('content')
    <h3>Edit Category</h3>
    
    <hr>
    
    <form action="{{ url('categories', $category->id) }}" method="POST">
        @method('PUT')
        @csrf
        
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="{{ $category->name }}">
        </div>

        <div class="form-group">
            <label>Description</label>
            <textarea name="description" rows="10" class="form-control">{{ $category->description }}</textarea>
        </div>

        <button class="btn btn-primary">Update Category</button>
    </form>
@endsection