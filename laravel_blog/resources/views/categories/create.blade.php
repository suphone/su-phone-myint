@extends('layout')

@section('content')
    <h3>Create New Category</h3>
    
    <hr>
    
    <form action="{{ url('categories') }}" method="POST">
        @csrf

        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name">
        </div>

        <div class="form-group">
            <label>Description</label>
            <textarea name="description" rows="10" class="form-control"></textarea>
        </div>

        <button class="btn btn-primary">Create New Category</button>
    </form>
@endsection