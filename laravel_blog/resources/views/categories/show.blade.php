@extends('layout')

@section('content')
    <h3>{{ $category->name }}</h3>
    <p>
        {{ $category->description }}
    </p>

    <ul>
        @foreach($category->products as $product)
            <li>{{ $product->name }}</li>
        @endforeach
    </ul>
    <hr>
    <a href="{{ url('categories') }}" class='btn btn-primary'>Categories</a>
@endsection