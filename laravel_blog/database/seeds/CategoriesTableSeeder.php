<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
        $category = new Category();
        $category->name = "Test Category 1";
        $category->description = str_random(1000);
        $category->save();

        $category2 = new Category();
        $category2->name = "Test Category 2";
        $category2->description = str_random(1000);
        $category2->save();

        $category3 = new Category();
        $category3->name = "Test Category 3";
        $category3->description = str_random(1000);
        $category3->save();
    }
}
